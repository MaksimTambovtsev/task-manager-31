package ru.tsc.tambovtsev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.dto.request.ServerAboutRequest;
import ru.tsc.tambovtsev.tm.dto.request.ServerVersionRequest;
import ru.tsc.tambovtsev.tm.dto.response.ServerAboutResponse;
import ru.tsc.tambovtsev.tm.dto.response.ServerVersionResponse;

public interface ISystemEndpoint {

    @NotNull ServerAboutResponse getAbout(@NotNull ServerAboutRequest request);

    @NotNull ServerVersionResponse getVersion(@NotNull ServerVersionRequest request);

}
