package ru.tsc.tambovtsev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.model.Project;
import ru.tsc.tambovtsev.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-remove-by-id";

    @NotNull
    public static final String DESCRIPTION = "Remove project by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable final String userId = getUserId();
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final Project project = getProjectService().findById(userId, id);
        getProjectTaskService().removeProjectById(userId, project.getId());
    }

}
