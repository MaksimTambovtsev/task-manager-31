package ru.tsc.tambovtsev.tm.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.component.Server;

public abstract class AbstractServerTask implements Runnable {

    @NotNull
    protected Server server;

    public AbstractServerTask(@NotNull final Server server) {
        this.server = server;
    }

}
