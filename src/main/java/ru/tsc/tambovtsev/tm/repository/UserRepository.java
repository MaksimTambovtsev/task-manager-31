package ru.tsc.tambovtsev.tm.repository;

import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.IUserRepository;
import ru.tsc.tambovtsev.tm.model.Task;
import ru.tsc.tambovtsev.tm.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User removeUser(@Nullable final User user) {
        models.remove(user);
        return user;
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        return models.stream()
                .filter(item -> login.equals(item.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        return models.stream()
                .filter(item -> email.equals(item.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User removeById(@Nullable final String id) {
        @Nullable final User user = findById(id);
        if (user == null) return null;
        return removeUser(user);
    }

    @Nullable
    @Override
    public User removeByLogin(final String login) {
        @Nullable final User user = findByLogin(login);
        if (user == null) return null;
        return removeUser(user);
    }

}

