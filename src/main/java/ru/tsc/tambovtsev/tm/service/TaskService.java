package ru.tsc.tambovtsev.tm.service;

import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.repository.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.service.ITaskService;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.exception.AbstractException;
import ru.tsc.tambovtsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.tambovtsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.tambovtsev.tm.exception.field.*;
import ru.tsc.tambovtsev.tm.model.Project;
import ru.tsc.tambovtsev.tm.model.Task;

import java.util.*;

public class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@Nullable final ITaskRepository repository) {
        super(repository);
    }

    @Nullable
    @Override
    public Task create(@Nullable final String userId, @Nullable final String name) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        return repository.create(userId, name);
    }

    @Nullable
    @Override
    public Task create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        return repository.create(userId, name, description);
    }

    @Nullable
    @Override
    public Task create(@Nullable final String userId, @Nullable final String name,
                       @Nullable final String description, @Nullable final Date dateBegin,
                       @Nullable final Date dateEnd) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        final Task task = create(userId, name, description);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        return task;
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).orElseThrow(ProjectNotFoundException::new);
        return repository.findAllByProjectId(userId, projectId);
    }

    @Nullable
    @Override
    public Task updateById(@Nullable final String userId, @Nullable final String id,
                           @Nullable final String name, @Nullable final String description) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        @Nullable final Task task = findById(userId, id);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return task;
    }

    @Nullable
    @Override
    public Task changeTaskStatusById(@Nullable final String userId, @Nullable final String id,
                                     @Nullable final Status status) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        final Task task = findById(userId, id);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        task.setStatus(status);
        task.setUserId(userId);
        return task;
    }

}
